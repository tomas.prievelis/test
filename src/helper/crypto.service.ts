import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class CryptoService {
    generateCode(stringLength: number): string {
        return crypto
            .randomBytes(Math.ceil(stringLength / 2))
            .toString('hex')
            .slice(0, stringLength);
    }
    
    encHashSha512(data: crypto.BinaryLike, salt: string): string {
        const hash: crypto.Hmac = crypto.createHmac('sha512', salt);
        hash.update(data);
        return hash.digest('hex');
    }
}