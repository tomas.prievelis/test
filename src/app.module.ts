import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { CompanyModule } from './company/company.module';
import { VacanciesModule } from './vacancies/vacancies.module';
require('dotenv').config();

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb+srv://${process.env.DB_MONGO_USER}:${process.env.DB_MONGO_PASSWORD}@cluster0.7zgkp.mongodb.net/test`, {
      connectionName: 'companyDefault',
    }),
    VacanciesModule,
    UsersModule,
    CompanyModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

