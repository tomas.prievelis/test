import { Controller, Get, HttpCode } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('system')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('healthcheck')
  @HttpCode(200)
  async healthcheckControllerWithOutPrefix() {
    const used = process.memoryUsage().heapUsed / 1024 / 1024;
    return {
      environment: `${process.env.NODE_ENV || 'unknown'}`,
      dateTime: `${new Date()}`,
      isHealthy: true,
      heap: `The script uses approximately ${Math.round(used * 100) / 100} MB`,
    };
  }
}