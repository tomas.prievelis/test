import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCompanyDto {
    @ApiProperty({ default: '<company name>' })
    @IsString()
    name: string;

    @ApiProperty({ default: '<company address>' })
    @IsString()
    @IsNotEmpty()
    address: string;
}
