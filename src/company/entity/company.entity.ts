import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Company extends Document {
    @Prop()
    id: string;

    @Prop()
    name: string;

    @Prop()
    address: string;
    
    @Prop()
    createdAt: Date;
}

export const CompanySchema = SchemaFactory.createForClass(Company);