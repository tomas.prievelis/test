import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCompanyDto } from './dto/create-company.dto';
import { Company } from './entity/company.entity';
 
@Injectable()
export class CompanyService {
  constructor(
    @InjectModel('Company') private readonly companyRepository: Model<Company>,
  ) {}

  async create(createCompanyDto: CreateCompanyDto): Promise<any> {
    const company = {
        ...createCompanyDto,
        createdAt: new Date(),
      };
    await new this.companyRepository(company).save();
    return { description: 'Company saved'};
  }
}