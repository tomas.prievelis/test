export enum UserStatus {
    USER_CREATED = 'user_created',
    USER_DELETED = 'user_deleted',
    USER_BLOCKED = 'user_blocked',
}