import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import { UserStatus } from '../userStatus.enum';

@Schema()
export class User extends Document {
    @Prop()
    id: string;

    @Prop()
    name: string;

    @Prop()
    userName: string;

    @Prop()
    companyName: string;

    @Prop({
        default: "615d9bcdc6db8b38c0dc8939",
    })
    companyId: string;

    @Prop()
    role: string;

    @Prop()
    password: string;

    @Prop()
    token: string;

    @Prop()
    salt: string;

    @Prop({
        type: String,
        enum: UserStatus,
        default: UserStatus.USER_CREATED,
    })
    status: string;

    @Prop()
    createdAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);