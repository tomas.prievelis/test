import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {
    @ApiProperty({ default: '<your name>' })
    @IsString()
    name: string;

    @ApiProperty({ default: '<your user name>' })
    @IsString()
    @IsNotEmpty()
    userName: string;

    @ApiProperty({ default: '<your password>' })
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiProperty({ default: '<your company name>' })
    @IsString()
    companyName: string;

    @ApiProperty({ default: '<add role>' })
    @IsString()
    role: string;
}
