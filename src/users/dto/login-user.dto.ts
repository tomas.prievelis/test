import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class LoginUserDto {

    @ApiProperty({ default: '<your user name>' })
    @IsString()
    userName: string;

    @ApiProperty({ default: '<your password>' })
    @IsString()
    password: string;
}