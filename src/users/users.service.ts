import {Injectable} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './entity/user.entity';
import { LoginUserDto } from './dto/login-user.dto';
import { CryptoService } from 'src/helper/crypto.service';
import { UserStatus } from './userStatus.enum';
import * as moment from 'moment';
import { v4 as uuidv4 } from 'uuid';


@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private readonly userRepository: Model<User>,
    private readonly cryptoService: CryptoService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<{ user: string, status: string }> {

    const users: User[] = await this.userRepository.find({
          select: [ 'userName' ],
          where: { companyName: createUserDto.companyName, userName: createUserDto.userName }
    });

    users.map((user: User) => {
      if(user.userName === createUserDto.userName && user.companyName === createUserDto.companyName) throw new Error('User already exists, please login');
    });

    const salt: string = this.cryptoService.generateCode(64);
    const password: string = this.cryptoService.encHashSha512(createUserDto.password, salt);

    const token = `123456789+${uuidv4()}`
    let newUser: any = {
        name: createUserDto.name,
        userName: createUserDto.userName,
        companyName: createUserDto.companyName,
        role: createUserDto.role,
        status: UserStatus.USER_CREATED,
        password: password,
        salt: salt,
        token: token,
        createdAt: moment.utc().toDate(),
    }


    const registerUser: any = await new this.userRepository(newUser).save(newUser);
    if (!registerUser) throw new Error('User was not created!');
    return { user: registerUser.userName, status: registerUser.status };
  }

  async login(loginUserDto: LoginUserDto): Promise<any> {

    const users: User[] = await this.userRepository.find({
      select: ['salt'],
      where: { userName: loginUserDto.userName }
    });

    const token = `${uuidv4()}-${uuidv4()}`;
    await Promise.all(users.map(async user => {
      if (user.userName === loginUserDto.userName) {
        const password: string = this.cryptoService.encHashSha512(loginUserDto.password, user.salt);
        await this.userRepository.updateOne({userName: user.userName},{token: token})
      if(user.password !== password) throw new Error('Password is incorrect');
      };
    })
  );
    return `Welcome, ${loginUserDto.userName}! Your accessToken is: ${token}`;
  };

  async validate(token: string): Promise<any> {
    const accessToken = await this.userRepository.find({ token: token });
    accessToken.map(user => {
      if(user.token === token && user.role === 'admin'){
        return user.token
      };
      if(user.token !== token || user.role === 'client'){
        return 'Access denied'
      };
    })
    return {};
  }
}

