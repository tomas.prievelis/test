import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CryptoService } from 'src/helper/crypto.service';
import { UserSchema } from './entity/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }], 'companyDefault'),
  ],
  controllers: [UsersController],
  providers: [UsersService, CryptoService],
  exports: [UsersService]
})
export class UsersModule {}