import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import { UsersModule } from './users/users.module';
import { CompanyModule } from './company/company.module';
import { VacanciesModule } from './vacancies/vacancies.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(function(req, res, buf, next) {
    req.rawBody = buf.toString();
    next();
  });

  app.use(function(req, res, next) {
    res.header('x-powered-by', 'Alvita');
    res.header('Cache-Control', 'no-cache');
    res.header('Pragma', 'no-cache');
    next();
  });

  const config = new DocumentBuilder()
  .setTitle('PredictiveHire')
  .setDescription(`version - ${new Date()}`)
  .setVersion('1.000.1')
  .addTag(`${new Date()}`)
  .build();

  const document = SwaggerModule.createDocument(app, config, {
    include: [
      UsersModule,
      CompanyModule,
      VacanciesModule,
    ],
  });
  SwaggerModule.setup('api/v1/swagger', app, document);

  app.use(helmet());
  app.enableCors();
  await app.listen(process.env.APP_PORT);
  console.log(`service was started on ${process.env.APP_PORT}`);
}
bootstrap();
