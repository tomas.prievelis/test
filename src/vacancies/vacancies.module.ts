import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';
import { VacanciesSchema } from './entity/vacancies.entity';
import { VacanciesController } from './vacancies.controller';
import { VacanciesService } from './vacancies.service';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([{ name: 'Vacancies', schema: VacanciesSchema }], 'companyDefault'),
  ],
  controllers: [VacanciesController],
  providers: [VacanciesService],
})
export class VacanciesModule {}