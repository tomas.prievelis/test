import { Controller, Post, Body, UsePipes, ValidationPipe, Get, Put, Param, Delete } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { UsersService } from 'src/users/users.service';
import { CreateVacancyDto } from './dto/create-vacancy.dto';
import { VacanciesService } from './vacancies.service';


@ApiTags('Vacancies')
@Controller('/api/v1/vacancies')
export class VacanciesController {
  constructor(private vacanciesService: VacanciesService,
    private readonly usersService: UsersService)
  {}

  @Post('create/:token')
  @ApiBody({ type: CreateVacancyDto })
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(
    @Body() createVacancy: CreateVacancyDto,
    @Param('token') token: string,
  ) {
    return this.vacanciesService.create(createVacancy, token);
  }

  @Get('all')
  async getVacancies() {
    return await this.vacanciesService.getVacancies();
  }

  @Put(':vacancyId/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateVacancy(
    @Param('vacancyId') vacancyId: string,
    @Body() body: CreateVacancyDto,
  ) {
    return await this.vacanciesService.updateVacancy(vacancyId, body);
  }

  @Delete(':vacancyId/delete')
  async removeOne(@Param('vacancyId') vacancyId: string) {
    return await this.vacanciesService.deleteOne(vacancyId);
  }
}