import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UsersService } from 'src/users/users.service';
import { CreateVacancyDto } from './dto/create-vacancy.dto';
import { Vacancies } from './entity/vacancies.entity';

@Injectable()
export class VacanciesService {
  constructor(
    @InjectModel('Vacancies') private readonly vacanciesRepository: Model<Vacancies>, private usersService: UsersService,
  ) {}

  async create(createVacancy: CreateVacancyDto, token: string): Promise<any> {
    await this.usersService.validate(token);    
    const vacancy = {
        ...createVacancy,
        createdAt: new Date(),
      };
    await new this.vacanciesRepository(vacancy).save();
    return { description: 'Vacancy saved'};
  }

  async getVacancies(): Promise<any> {
    return await this.vacanciesRepository.find();
  }

  async updateVacancy(vacancyId: string, body: CreateVacancyDto): Promise<any> {
    return await this.vacanciesRepository.updateOne({ 
      _id: vacancyId 
      },
      { ...body }
    );
  }

  async deleteOne(vacancyId: string) {
    const actionResult = await this.vacanciesRepository.deleteOne({
      _id: vacancyId,
    });
    return { 
      deleted: actionResult.deletedCount,
      description: 'Vacancy succesfully deleted',
    }
  }
}