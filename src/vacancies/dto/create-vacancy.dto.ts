import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateVacancyDto {
    @ApiProperty({ default: '<vacancy title>' })
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiProperty({ default: '<vacancy description>' })
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty({ default: '<vacancy expiration date, e.g. 2021-10-06T12:00:00.000+00:00>' })
    @IsNotEmpty()
    expiredAt: Date;
}
