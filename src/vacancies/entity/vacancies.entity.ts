import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Vacancies extends Document {
    @Prop()
    id: string;

    @Prop({
        type: String,
        default: 'PredictiveHire',
    })
    companyName: string;

    @Prop()
    title: string;

    @Prop()
    description: string;
    
    @Prop()
    createdAt: Date;

    @Prop()
    expiredAt: Date;
}

export const VacanciesSchema = SchemaFactory.createForClass(Vacancies);